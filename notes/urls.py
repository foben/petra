from django.conf.urls import patterns, url
from notes import views


urlpatterns = patterns('',
    url(r'^(?P<pk>\d+)', views.NoteDetailView.as_view(), name='detail'),
    )


