from django.views import generic
from notes.models import Note

class NoteDetailView(generic.DetailView):
    model = Note

