from django.db import models
from customers.models import Customer

class Note(models.Model):
    date = models.DateTimeField('start date')
    title = models.CharField('Title', max_length=200)
    text = models.CharField('Text', max_length=99999)
    customer = models.ForeignKey(Customer)

    def get_feed_template(self):
        return "feeditems/feeditem_note.html"

    def get_sort_date(self):
        return self.date
    
    def get_filteritem(self):
        return "note"
