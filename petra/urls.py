from django.conf.urls import patterns, include, url
from petra import views
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from django.views.generic import TemplateView

urlpatterns = patterns('',
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^$', TemplateView.as_view(template_name="base.html")),
    url(r'^appointments/', include('appointments.urls', namespace='appointments')),
    url(r'^customers/', include('customers.urls', namespace='customers')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^invoices/', include('invoices.urls', namespace='invoices')),
    url(r'^notes/', include('notes.urls', namespace='notes')),
)
