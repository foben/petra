from django.db import models
from django.utils import timezone

class Customer(models.Model):
    last_name       = models.CharField(max_length=200)
    first_name  = models.CharField(max_length=200)
    birthday   = models.DateField()

    def ordered_appointment_list(self):
        return self.appointment_set.order_by('-start_date')

    def next_appointment(self):
        laterThanNow = self.appointment_set.filter(start_date__gte=timezone.now()).order_by('start_date')
        return laterThanNow[0].start_date if len(laterThanNow) > 0 else None

    def __unicode__(self):
        res = self.first_name + " " + self.last_name
        return res

