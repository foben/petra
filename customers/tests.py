"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase

from customers.models import Customer
from appointments.models import Appointment

from datetime import timedelta
from django.utils import timezone

now = timezone.now()
tomorrow = now + timedelta(days=1)
yesterday = now - timedelta(days=1)
def create_customer():
    return Customer.objects.create(name='testName',firstName='testFirstName',birthday='1987-10-22')

class CustomerTest(TestCase):
    def test_correct_next_appointment(self):
        """
        Tests that nextAppointment() returns the next appointment in the future
        """
        c = create_customer()
        in3days = now + timedelta(days=3)
        c.appointment_set.create(title='yesterday',start_date=yesterday,end_date=now)
        c.appointment_set.create(title='tomorrow',start_date=tomorrow,end_date=in3days )
        c.appointment_set.create(title='in3days',start_date=in3days,end_date=in3days + timedelta(hours=2) )
        self.assertEqual(c.nextAppointment(), tomorrow)

    def test_no_next_appointment(self):
        """
        Tests that nextAppointment() returns no appointment in case there are only past appointments
        """
        c = create_customer()
        c.appointment_set.create(title='yesterday',start_date=yesterday,end_date=now)
        self.assertEqual(c.nextAppointment(), None)

