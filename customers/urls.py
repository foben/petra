from django.conf.urls import patterns, url
from customers import views

urlpatterns = patterns('',
    url(r'^$', views.IndexView.as_view(), name='index'), 
    url(r'^feed/(?P<cust>\d+)/', views.CustomerFeedView.as_view(), name='customerfeed'),
)
