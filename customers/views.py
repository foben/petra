from django.views import generic
from django.shortcuts import get_object_or_404
from appointments.models import Appointment
from customers.models import Customer
from notes.models import Note
from customers.models import Customer


class IndexView(generic.ListView):
    # default template_name = <app name>/<model name>_list.html
    # default context_object_name = <model name>_list
    def get_queryset(self):
        return Customer.objects.all()

class CustomerFeedView(generic.ListView):
    template_name = "customers/customer_feed.html"
    def get_queryset(self):
        customer_id = self.kwargs['cust']
        get_object_or_404(Customer, pk=customer_id)
        items = list(Appointment.objects.filter(customer=customer_id))
        items += (list(Note.objects.filter(customer=customer_id)))
        items.sort(lambda x,y: cmp(x.get_sort_date(), y.get_sort_date()), reverse=True)
        return items
    
    def get_context_data(self, **kwargs):
        context = super(CustomerFeedView, self).get_context_data(**kwargs)
        context['all_customers'] = Customer.objects.all()
        return context
