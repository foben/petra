function appendDateTimePicker(element) {
    var dtopts = {
        "firstDayOfWeek": 1,
        "closeOnSelected": true
    };
    // if we use jQuery UI anyway we could use their date picker
    $(element).appendDtpicker(dtopts);    
}

$( document ).ready(function (e){
  appendDateTimePicker('#id_start_date');
  appendDateTimePicker('#id_end_date');
});
