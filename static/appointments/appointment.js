/*global $:false, jQuery:false */
$(document).ready(function() {
  "use strict";

  // Appointment-related variables used throughout the following functions
  var createApptUrl = 'create',
      createApptFormId = '#create-appointment-form',
      createApptSaveId = '#appointment-save',
      $apptDialogContainer = $("#appointment-dialog"),
      screenWidthThreshold = 514;

  /*
   * Load fullcalendar
   */ 
  var calendar = $('#calendar').fullCalendar({
    // Options, callbacks and methods are ordered along official docs categories from http://arshaw.com/fullcalendar/docs
     
    // General display
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    firstDay: 1, // Monday=1
    viewRender: function(view, element) { // event such as click on '>' has occurred
      // copy header title html to dedicated div
      var headerTitleHtml = $('td .fc-header-title')[0].outerHTML;
      $('#cal-title').html(headerTitleHtml);
      showAppropriateTitle();
      adaptCalenderHeight(); 
    },
    windowResize: function(view) {
      showAppropriateTitle();
      adaptCalenderHeight(); 
    },

    // Views
    defaultView: 'agendaDay', // http://arshaw.com/fullcalendar/docs/views/defaultView
    // defaultView: $(window).width() < 514 ? 'basicDay' : 'month',

    // Agenda Options
    allDaySlot: false,
    allDayText: '', 
    axisFormat: 'H:mm', // 05:00
    // defaultEventMinutes //??

    // Text/Time Customization
    timeFormat: {
      agenda: 'H:mm{ - H:mm}', // 5:00 - 16:30 for agendaWeek and agendaDay
      '': 'H:mm' // 16:30 for all other views
    },
    columnFormat: {
      month: 'ddd',    // Mon
      week: 'ddd d.M', // Mon 7.9
      day: 'dddd d.M'  // Monday 7.9
    },
    titleFormat: {
      month: 'MMMM yyyy',  // September 2009
      week: "d. [ MMM.] [ yyyy]{ '&#8212;' d. MMM. yyyy}", // 7. - 13. Sep. 2009
      day: 'dddd, d. MMM. yyyy'  // Tuesday, 8. Sep. 2009
    },
    buttonText: {
      today:    'Heute',
      month:    'Monat',
      week:     'Woche',
      day:      'Tag'
    },
    monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
    monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
    dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
    dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],

    // Selection
    selectable: true,
    selectHelper: true,
    unselectAuto: false,
    select: function(startDate, endDate, allDay) {
      showAppointmentDialog(startDate, endDate, allDay);
    },

    // Event Data
    events: "events", // URL of the JSON feed that the calendar fetches Event Objects from
    loading: function(bool) {
      //TODO add loading element if wanted or remove
      if (bool) $('#loading').show();
        else $('#loading').hide();
    },

    // Event Rendering
    eventRender: function(eventObj, element) {
      //TODO display notes somehow (in tooltip or similar, using e.g. bootstrap tooltips)
    },

    // Event Dragging & Resizing
    editable: true,
    eventDrop: function(eventObj, dayDelta, minuteDelta, allDay, revertFunc) {
      // Triggered when dragging stops and the event has moved to a different day/time.
      // event drop means start and end time have changed
      var startDayDelta = dayDelta;
      var endDayDelta = dayDelta;
      var startMinuteDelta = minuteDelta;
      var endMinuteDelta = minuteDelta;
      handleEventChanges(eventObj, startDayDelta, startMinuteDelta, endDayDelta, endMinuteDelta, revertFunc, allDay);
    },
    eventResize: function(eventObj, dayDelta, minuteDelta, revertFunc) {
      // Triggered when resizing stops and the event has changed in duration.
      // event resize means only end time has changed
      var endDayDelta = dayDelta;
      var endMinuteDelta = minuteDelta;
      handleEventChanges(eventObj, 0, 0, endDayDelta, endMinuteDelta, revertFunc);
    }
  });

  /*
   * Change calender height so that it ends on the bottom of the screen.
   */
  function adaptCalenderHeight() {
      // calendarHeight = viewport height - calendar position 
      var yPosition = $('#calendar').offset().top;
      var overallHeight = $(window).height();
      var height = overallHeight - yPosition;
      $('#calendar').fullCalendar('option', 'height', height);
  }

  /*
   * Show the calendar title that is appropriate for the screen size (viewport) and hide the other title.
   */ 
  function showAppropriateTitle() {
      var $headerTitle = $('td .fc-header-title');
      var $calTitle = $('#cal-title').children();
      var isSmallScreen = $(window).width() < screenWidthThreshold;
      if (isSmallScreen){
        $headerTitle.hide();
        $calTitle.show();
      }
      else {
        $headerTitle.show();
        $calTitle.hide();
      } 
  }

  function showAppointmentDialog(startDate, endDate, allDay) {
    loadCreateAppointmentForm(startDate, endDate);
    // create and open appointment dialog
    $apptDialogContainer.dialog({
      title: "Termin anlegen",
      height: "auto",
      width: 400, // auto width does not work correctly
      modal: true,
      close: function(event, ui) {
        calendar.fullCalendar('unselect');
      }
    }); 
  }

  // submit form as ajax call on button click
  // $('...').click(...) does not work in this case since button was dynamically added (http://stackoverflow.com/a/11878976/1501100)   
  // single event handler can be registered even though #appointment-save input has not been loaded yet.
  $(document).on('click', createApptSaveId, function(event) {
    event.preventDefault();
    var $form = $(createApptFormId);
    $.ajax({
      url: createApptUrl,
      type: 'POST',
      data: $form.serialize()
    }).done(function (data, textStatus, jqXHR) {
      // HACKISH
      // check if we have the same form in the response again (this means there was no success 302 redirect to the main page)
      var $respForm = $(data).find(createApptFormId);
      if ($respForm.length !== 0) {
        $apptDialogContainer.html($respForm);
        appendDateTimePicker('#id_start_date');
        appendDateTimePicker('#id_end_date');
        return;
      }
      // success
      $apptDialogContainer.dialog("close"); // close before destroy to trigger cleanup methods
      $apptDialogContainer.dialog("destroy");
      calendar.fullCalendar('refetchEvents');
      $apptDialogContainer.empty();
    }).fail(function (jqXHR, textStatus, errorThrown){
      handleAjaxFail(jqXHR, textStatus, errorThrown);
    });
  });

  /*
   * Load create appointment form into appointment dialog div
   */
  function loadCreateAppointmentForm(startDate, endDate) {
    $apptDialogContainer.load(createApptUrl + ' ' + createApptFormId, function() {
      var formatString = 'yyyy-MM-dd HH:mm:ss',
          startDateString = $.fullCalendar.formatDate(startDate, formatString),
          endDateString = $.fullCalendar.formatDate(endDate, formatString);
      // set start and end dates according to params
      var $startDate = $('input#id_start_date').val(startDateString);
      var $endDate = $('input#id_end_date').val(endDateString);
      // register event handlers
      appendDateTimePicker('#id_start_date');
      appendDateTimePicker('#id_end_date');
    });
  }

  function handleAjaxFail(jqXHR, textStatus, errorThrown) {
    // TODO show user-friendly message in a dedicated error panel
    alert("The following error occured: " + textStatus + " " + errorThrown);
  }

  /*
   * Handle start and end time changes of events that occur when dragging or resizing.
   */
  function handleEventChanges(eventObj, startDayDelta, startMinuteDelta, endDayDelta, endMinuteDelta, revertFunc, allDay) {
    if (allDay) {
      //TODO is this supported?
      console.log("Event is now all-day");
    }

    var data = {
      "apptId": eventObj.id,
      "startDayDelta": startDayDelta,
      "startMinuteDelta": startMinuteDelta,
      "endDayDelta": endDayDelta,
      "endMinuteDelta": endMinuteDelta
    };

    $.ajax({
      type: "POST",
      url: "events/changes",
      data: data,
    }).done(function (response, textStatus, jqXHR){
      // do nothing atm
    }).fail(function (jqXHR, textStatus, errorThrown){
      handleAjaxFail(jqXHR, textStatus, errorThrown);
      revertFunc();
    });
  }
});
