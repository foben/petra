$(document).ready(function(e){
    var filters = {'#filter_notes': '.filteritem_note',
        '#filter_appointments': '.filteritem_appointment'};
    $.each(filters, function(i, v){
        $(i).click(function(e){
            var isInactive = $(e.currentTarget).hasClass("filter_inactive");
            if(isInactive){
                $(e.currentTarget).removeClass("filter_inactive");
                blockDisplay(v);
            }
            else{
                $(e.currentTarget).addClass("filter_inactive");
                noDisplay(v);
            }
        });
    });

    function blockDisplay(identifier){
        $(identifier).each(function(i, v){
            $(v).css('display', 'block');
        });
    }

    function noDisplay(identifier){
        $(identifier).each(function(i, v){
            $(v).css('display', 'none');
        });
    }

});
