from django.conf.urls import patterns, url
from invoices import views

urlpatterns = patterns('',
        url(r'^create', views.create_invoice, name='create_invoice'),
        url(r'^(?P<pk>\d+)', views.DetailView.as_view(), name='detail'),
        url(r'^', views.InvoiceListView.as_view(), name='list'),
        )
