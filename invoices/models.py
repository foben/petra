from django.db import models
from customers.models import Customer
from appointments.models import Appointment

class Invoice(models.Model):
    from_date = models.DateTimeField('from date')
    until_date   = models.DateTimeField('until date')
    customer = models.ForeignKey(Customer)

    def get_appointments(self):
        return Appointment.objects.filter(invoice=self.id)

    def get_total(self):
        sum = 0
        for apt in self.get_appointments():
            sum += apt.get_appointment_price()
        return sum
