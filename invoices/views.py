import datetime
import json
from django.http import HttpResponse
from django.shortcuts import render
from django.views import generic
from django.core.serializers.json import DjangoJSONEncoder
from appointments.models import Appointment
from customers.models import Customer
from invoices.models import Invoice

def get_appointments(from_date, until_date, customer):
    appoints = Appointment.objects.filter(start_date__gte=from_date) \
            .filter(start_date__lte=until_date) \
            .filter(customer=customer.id) \
            .filter(invoice=None) \
            .filter(has_occured=True)
    return appoints


k_from_date = 'from_date'
k_until_date = 'until_date'
k_customer = 'for_customer'
k_appointments = 'appointments_to_invoice'
k_save_invoice = 'save_invoice'
k_total_sum = 'total_sum'
date_format = '%Y-%m-%d'

def from_session(session):
    from_date = datetime.datetime.strptime(session[k_from_date], date_format) 
    until_date = datetime.datetime.strptime(session[k_until_date], date_format)
    until_date += datetime.timedelta(days=1)
    customer = Customer.objects.get(pk=session[k_customer])
    appointments = get_appointments(from_date, until_date, customer) 
    sum = 0
    for apt in appointments:
        sum += apt.get_appointment_price()
    return {
            k_from_date: from_date, 
            k_until_date: until_date,
            k_customer: customer,
            k_appointments: appointments,
            k_total_sum: sum
           }
    

def create_invoice(request):
    ## Parameters for Template
    appointments_to_invoice = None 
    total_sum = -1
    customers = Customer.objects.all()
    error_message = None
    success_message = None

    if request.method == 'GET':
        #Not really anything to do here ...
        print 'GET'

    elif request.method == 'POST':
        if k_save_invoice in request.POST and request.POST[k_save_invoice]:
            dict = from_session(request.session)
            invoice = Invoice(
                        from_date=dict[k_from_date],
                        until_date=dict[k_until_date],
                        customer=dict[k_customer]
                        )
            invoice.save()
            for apt in dict[k_appointments]:
                apt.invoice = invoice
                apt.save()

            print 'SAVE'
        #Selection of appointments for invoice
        elif k_from_date in request.POST  and request.POST[k_from_date] and \
                k_until_date in request.POST and request.POST[k_until_date] and \
                k_customer in request.POST:
                    print request.POST
                    request.session[k_from_date] = request.POST[k_from_date]
                    request.session[k_until_date] = request.POST[k_until_date]
                    request.session[k_customer] = request.POST[k_customer]
                    dict = from_session(request.session)
                    appointments_to_invoice = dict[k_appointments]
                    total_sum = dict[k_total_sum]
                    success_message = 'Appointments for {0} between {1} and {2}' \
                            .format(dict[k_customer], dict[k_from_date], dict[k_until_date])
        else:
            error_message = 'Please specify customer, start and end date!'
#
#               sum = 0
#                for apt in appointments_to_invoice:
#                    sum += apt.get_appointment_price()
#                total_sum = sum
#                success_message = 'Invoice items for {0} between {1} and {2}:' \
        #        .format(for_customer, from_date, until_date)
    return render(request, 'invoices/create_invoice.html',
            {
                'error_message': error_message,
                'success_message': success_message,
                'customers': customers,
                'appointments_to_invoice': appointments_to_invoice,
                'total_sum':total_sum
                })



class DetailView(generic.DetailView):
    model = Invoice

class InvoiceListView(generic.list.ListView):
    context_object_name = 'invoice_list'
    model = Invoice
