from django.conf.urls import patterns, url
from appointments import views
from django.views.generic import TemplateView

urlpatterns = patterns('',
#    url(r'^$', views.IndexView.as_view(), name='index'), 
#    # Month Example: /2013/11/04
#    url(r'^(?P<year>\d{4})/(?P<month>\d+)/(?P<day>\d+)', 
#        views.AppointmentDayArchiveView.as_view(), 
#        name='appointment_day_numeric'),
#    # Week Example: /2013/week/44
#    url(r'^(?P<year>\d{4})/week/(?P<week>\d+)$',
#        views.AppointmentWeekArchiveView.as_view(),
#        name="appointment_week"),
#    # Day Example: /2013/11
#    url(r'^(?P<year>\d{4})/(?P<month>\d+)$',
#        views.AppointmentMonthArchiveView.as_view(month_format='%m'),
#        name="appointment_month_numeric"),
    
    url(r'^$', TemplateView.as_view(template_name="appointments/appointment_index.html"), name='index'),
    url(r'^events$', views.AppointmentJson.as_view(), name='eventsjson'),
    url(r'^events/changes$', views.AppointmentChanges.as_view(), name='eventschanges'),

    url(r'^(?P<pk>\d+)', views.DetailView.as_view(), name='detail'),

    url(r'^create', views.AppointmentCreate.as_view(), name='create'),
    url(r'^update/(?P<pk>\d+)', views.AppointmentUpdate.as_view(), name='update'),
    url(r'^delete/(?P<pk>\d+)', views.AppointmentDelete.as_view(), name='delete'),

)
