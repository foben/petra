from appointments.models import Appointment

import datetime, calendar, json
from datetime import timedelta
import pprint

from django.views import generic
from django.views.generic.base import TemplateView, View
from django.views.generic.dates import DayArchiveView, WeekArchiveView, MonthArchiveView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import get_object_or_404
from django.http import HttpResponse

class DetailView(generic.DetailView):
    # default template_name = <app name>/<model name>_detail.html
    model = Appointment

class AppointmentCreate(CreateView):
    model = Appointment
    # default template name: appointment_form.html
    def get_success_url(self):
        success_url = reverse('appointments:detail', args=(self.object.id,))
        print success_url
        return success_url
    def get_context_data(self, **kwargs):
        context = super(AppointmentCreate, self).get_context_data(**kwargs)
        context['action'] = 'create'
        return context

class AppointmentUpdate(UpdateView):
    model = Appointment
    # default template name: appointment_form.html
    def get_success_url(self):
        success_url = reverse('appointments:detail', args=(self.object.id,))
        print success_url
        return success_url
    def get_context_data(self, **kwargs):
        context = super(AppointmentUpdate, self).get_context_data(**kwargs)
        context['action'] = 'update'
        return context

class AppointmentDelete(DeleteView):
    model = Appointment
    success_url = reverse_lazy('appointments:index')


"""
Transform an appointment entity to an event object as needed by fullcalendar 
Event object specification: http://arshaw.com/fullcalendar/docs/event_data/Event_Object/
"""
def appt_as_event(appt):
    event = {}
    event["id"] = appt.pk
    event["title"] = appt.title
    # start = event.start_date.strftime("%Y-%m-%d") # %H:%M:%S")
    event["start"] = calendar.timegm(appt.start_date.utctimetuple())
    event["end"] = calendar.timegm(appt.end_date.utctimetuple())
    event["allDay"] = False
    event["url"] = reverse('appointments:detail', args=(appt.pk,))
    event["editable"] = True
    event["notes"] = appt.notes
    return event

def appts_as_events(appts):
    return [appt_as_event(appt) for appt in appts]

class AppointmentJson(View):
    def get(self, request):
        start = request.GET['start']
        start_date = datetime.date.fromtimestamp(int(start))
        end = request.GET['end']
        end_date = datetime.date.fromtimestamp(int(end))
        appts = Appointment.objects.filter(start_date__gte=start_date, end_date__lte=end_date)
        events = appts_as_events(appts)
        return HttpResponse(json.dumps(events), content_type="application/json")

def parse_params_as_ints(request, param_names):
    return {param: int(request.POST[param]) for param in param_names}

class AppointmentChanges(View):
    def post(self, request):
        appt = get_object_or_404(Appointment, pk=request.POST['apptId'])
        deltas = parse_params_as_ints(request, ['startDayDelta', 'startMinuteDelta', 'endDayDelta', 'endMinuteDelta'])
        if (deltas['startDayDelta'] != 0) or (deltas['startMinuteDelta'] != 0):
            appt.start_date = appt.start_date + timedelta(days=deltas['startDayDelta']) + timedelta(minutes=deltas['startMinuteDelta'] )
        if (deltas['endDayDelta'] != 0) or (deltas['endMinuteDelta'] != 0):
            appt.end_date = appt.end_date + timedelta(days=deltas['endDayDelta']) + timedelta(minutes=deltas['endMinuteDelta'])
        if any([v != 0 for v in deltas.values()]):
            appt.save()
        event = appt_as_event(appt)
        return HttpResponse(json.dumps(event), content_type="application/json")
