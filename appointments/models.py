from decimal import Decimal
from django.db import models
from customers.models import Customer
from pricings.models import Pricing
from django.core.urlresolvers import reverse

class Appointment(models.Model):
    title     = models.CharField('Title', max_length=200)
    notes     = models.CharField('Notes', max_length=500)
    customer  = models.ForeignKey(Customer)
    start_date = models.DateTimeField('start date')
    end_date   = models.DateTimeField('end date')
    has_occured = models.NullBooleanField(null=True, blank=True)
    pricing = models.ForeignKey(Pricing)
    invoice = models.ForeignKey('invoices.Invoice', null=True, blank=True)
    
    def get_feed_template(self):
        return "feeditems/feeditem_appointment.html"

    def get_sort_date(self):
        return self.start_date

    def get_filteritem(self):
        return "appointment"

    def __unicode__(self):
        return self.title
    
    def is_invoiced(self):
        if(self.invoice): return True
        else: return False

    def get_duration(self):
        time_delta = self.end_date - self.start_date
        return time_delta.total_seconds()

    def get_duration_hours(self):
        hours = self.get_duration() /60 /60
        hours = round(hours, 2)
        return hours

    def get_duration_minutes(self):
        mins = self.get_duration() /60 
        mins = round(mins, 2)
        return mins


    def get_appointment_price(self):
        hours = self.get_duration_hours()
        rate = self.pricing.rate_hour
        result = Decimal(hours) * rate
        return round(result, 2)

    def get_absolute_url(self):
        # redirect after successfull form submit to created appointment
        return reverse('appointments:index')
