from django.contrib import admin
from appointments.models import Customer
from appointments.models import Appointment
from invoices.models import Invoice
from notes.models import Note


admin.site.register(Customer)
admin.site.register(Appointment)
admin.site.register(Invoice)
admin.site.register(Note)
