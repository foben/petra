from django.db import models

class Pricing(models.Model):
    name = models.CharField('Name', max_length=200)
    rate_hour = models.DecimalField(max_digits=6, decimal_places=2)
    
    def __unicode__(self):
        result = self.name + '({0})'.format(self.rate_hour)
        return result
